package hr.ferit.brunozoric.threadsdemo

import android.util.Log

class MyThread(
    val waitTime: Long,
    val repetitions: Int): Thread() {

    val TAG = "Thread"
    val message = "A sneaky background thread done."
    val startedMessage = "Started working in background."

    override fun run() {
        Log.d(TAG, startedMessage)
        try{
            for (i in 1..repetitions){
                Thread.sleep(waitTime)
            }
            Log.d(TAG, message)
        } catch (e: InterruptedException){
            Log.e(TAG, e.message)
        }
    }
}