package hr.ferit.brunozoric.threadsdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val TAG = "Thread"
    val finishedMessage = "Glorious main thread. Work done."
    val startedMessage = "Started working on main thread."

    val waitTime = 1000L;
    val repetitions = 10;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpUi()
    }

    private fun setUpUi() {
        mainThreadRunAction.setOnClickListener { runHardWorkOnUi() }
        backThreadRunAction.setOnClickListener { runHardWorkInBackground() }
    }

    private fun runHardWorkOnUi() {
        Log.d(TAG, startedMessage)
        try {
            for (i in 1..repetitions) {
                Thread.sleep(waitTime)
            }
            Log.d(TAG, finishedMessage)
        } catch (e: InterruptedException) {
            Log.e(TAG, e.message)
        }
    }

    private fun runHardWorkInBackground() {
        MyThread(waitTime, repetitions).start()
    }
}
